## Personal aliases and functions

## Mount and Unmount noweb08 home dir
alias mountportalhome='sshfs -p 3122 -o sshfs_sync -o reconnect -C noweb08:/home/pchoo ~/portal_home'
alias unmountportalhome='fusermount -u ~/portal_home'

## Mount and Unmount noweb08
alias mountportal='sshfs -p 3122 -o sshfs_sync -o reconnect -C noweb08:/var/www/pjc.portal.centiq.co.uk ~/portal'
alias unmountportal='fusermount -u -z ~/portal'

## Mount and Unmount nodev01 Platform
alias mountplatform='sshfs -p 3122 -o sshfs_sync -o reconnect -C nodev01:/var/www/pjc ~/platform'
alias unmountplatform='fusermount -u -z ~/platform'

## Mount and Unmount nocom04
alias mountdev='sshfs -p 3122 -o sshfs_sync -o reconnect -C nocom04:/var/www/pjc.monitiq.com/web ~/dev_web'
alias unmountdev='fusermount -u -z ~/dev_web'

## Mount and Unmount nocom04 API folder
alias mountdevapi='sshfs -p 3122 -o sshfs_sync -o reconnect -C nocom04:/var/www/api.monitiq.com ~/api'
alias unmountdevapi='fusermount -u -z ~/api'

# Mount all wrapper
function mountall() {
  if [ -z "$(ls -A ~/dev_web)" ]; then
    mountdev
  fi
  if [ -z "$(ls -A ~/api)" ]; then
    mountdevapi
  fi
  if [ -z "$(ls -A ~/portal)" ]; then
    mountportal
  fi
}

# Unmountall wrapper
function unmountall() {
  if [ "$(ls -A ~/dev_web)" ]; then
    unmountdev
  fi
  if [ "$(ls -A ~/api)" ]; then
    unmountdevapi
  fi
  if [ "$(ls -A ~/portal)" ]; then
    unmountportal
  fi
}


# Recursive find and grep function
function rfgrep() {
      find ./ -name "$1" |  xargs grep -n "$2"
}
export -f rfgrep

# Ember-CLI Aliases
alias nom='if [ -f "package.json" ]; then if [ -d "node_modules" ]; then rm -rf node_modules; fi; npm cache clean && npm install; else echo "No package.json found, cannot nom this folder"; fi'
alias bom='if [ -f "bower.json" ]; then if [ -d "bower_components" ]; then rm -rf bower_components; fi; bower cache clean && bower install; else echo "No bower.json found, cannot bom this folder"; fi'
